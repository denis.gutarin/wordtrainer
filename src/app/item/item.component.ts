import { WordsService } from './../shared/words.service';
import { Component, OnInit } from '@angular/core';
import { ShowState } from '../shared/word';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  readonly emptyText = 'No more words today';

  word: ShowState;
  constructor(public wordService: WordsService) {}

  ngOnInit(): void {
    this.wordService.showWord.subscribe((word) => {
      this.word = word;
    });
  }
}
