export const testData = [
  {
    original: 'cat',
    translation: 'кошка',
    synonyms: 'kitten',
  },
  {
    original: 'dog',
    translation: 'собака',
    synonyms: 'hound',
  },
  {
    original: 'table',
    translation: 'стол',
    synonyms: 'desk',
  },
  {
    original: 'cup',
    translation: 'кружка',
    synonyms: 'mug',
  },
];
