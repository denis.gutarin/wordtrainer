import { OnInit } from '@angular/core';
export interface WordState {
  id?: string;
  original: string;
  translation: string;
  synonyms: string;
  familiarity?: number;
  translationDate?: Date;
  originalDate?: Date;
}

export interface ShowState {
  first: string;
  second: string;
  third: string;
}

export enum Passed {
  not = 0,
  once = 1,
  twice = 2,
}

const deviders = {
  0: 100,
  1: 50,
  2: 25,
  3: 10,
  4: 5,
  5: 2,
  6: 1,
};

export class Word {
  state: WordState;
  constructor(obj: WordState) {
    const intitial: WordState = {
      id: '',
      original: '',
      translation: '',
      synonyms: '',
      familiarity: 0,
      originalDate: new Date(2000, 0, 1),
      translationDate: new Date(2000, 0, 1),
    };
    this.state = { ...intitial, ...obj };
  }

  toView(): ShowState {
    if (this.checkPassed() == Passed.once) {
      return {
        first: this.state.original,
        second: this.state.translation,
        third: this.state.synonyms,
      };
    } else {
      return {
        first: this.state.translation,
        second: this.state.original,
        third: this.state.synonyms,
      };
    }
  }

  getDevider(): number {
    return deviders[this.state.familiarity] || 1;
  }

  checkPassed(): Passed {
    const now = new Date();
    const nowDays = Math.floor(now.valueOf() / 86400000);
    const wordOriginalDays = Math.floor(
      this.state.originalDate.valueOf() / 86400000
    );
    const wordTranslationDays = Math.floor(
      this.state.translationDate.valueOf() / 86400000
    );
    return nowDays <= wordOriginalDays && nowDays <= wordTranslationDays
      ? Passed.twice
      : nowDays <= wordTranslationDays
      ? Passed.once
      : Passed.not;
  }

  private tryAddFamiliarity() {
    if (this.checkPassed() == Passed.twice) this.state.familiarity += 1;
  }

  private resetFamiliarity() {
    this.state.familiarity = 0;
  }

  private updateDate() {
    if (this.checkPassed() == Passed.not) {
      this.state.translationDate = new Date();
    } else {
      this.state.originalDate = new Date();
    }
  }

  rightAnswer() {
    this.tryAddFamiliarity();
    this.updateDate();
  }

  wrongAnswer() {
    this.resetFamiliarity();
    this.updateDate();
  }
}
