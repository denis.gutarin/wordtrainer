import { testData } from './words.dummy';
import { Injectable } from '@angular/core';
import { Observable, from, range, BehaviorSubject } from 'rxjs';
import { switchMap, map, toArray, take, flatMap, filter } from 'rxjs/operators';
import { Word, ShowState, Passed, WordState } from './word';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class WordsService {
  static readonly URL = 'https://wordtrainer-ef02b.firebaseio.com/words';

  allWords: Word[] = [];
  currentWord: BehaviorSubject<Word> = new BehaviorSubject(null);
  showWord: BehaviorSubject<ShowState> = new BehaviorSubject(null);
  passCounter = new BehaviorSubject(0);
  secondVisible = new BehaviorSubject(false);
  thirdVisible = new BehaviorSubject(false);

  constructor(private http: HttpClient) {
    this.allWords = testData.map((it) => new Word(it));
    this.currentWord.subscribe((word) => {
      this.showWord.next(word?.toView() ?? null);
    });
    this.next();
  }

  private fetchWords(): Word[] {
    
    testData.map((it) => new Word(it));
  }

  private getInArray(): Word {
    const inArray = this.allWords.find(
      (it) => it.state.original == this.currentWord.value.state.original
    );
    return inArray;
  }

  fix(right: boolean) {
    const word = this.getInArray();
    if (right) word.rightAnswer();
    else word.wrongAnswer();
    this.next();
  }

  save(word: Word) {
    return this.http.put<WordState>(
      `${WordsService.URL}/${word.state.id}.json`,
      word.state
    );
  }

  show() {
    if (this.thirdVisible.value) return;
    if (this.secondVisible.value) {
      this.thirdVisible.next(true);
      return;
    }
    this.secondVisible.next(true);
  }

  next() {
    from(this.allWords)
      .pipe(
        filter((word) => word.checkPassed() !== Passed.twice),
        switchMap((word) => {
          const times = word.getDevider();
          return range(0, times).pipe(map(() => word));
        }),
        toArray(),
        map((array) => {
          const index = Math.floor(Math.random() * array.length);
          return array[index];
        })
      )
      .subscribe((value) => {
        console.log(value);
        console.log(this.allWords);
        this.thirdVisible.next(false);
        this.secondVisible.next(false);
        this.currentWord.next(value);
      });
  }
}
