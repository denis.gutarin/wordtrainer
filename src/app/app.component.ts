import { WordsService } from './shared/words.service';
import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(public WordsService: WordsService) {}

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (event.keyCode == 13 || event.keyCode == 20) {
      this.WordsService.show();
      return;
    }
    if (event.keyCode == 37) {
      this.WordsService.fix(false);
      return;
    }
    if (event.keyCode == 39) {
      this.WordsService.fix(true);
      return;
    }
  }
}
